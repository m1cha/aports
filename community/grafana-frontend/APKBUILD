# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>

# Make sure to keep this package in sync with grafana.

# Frontend files split from main grafana package.
# This is because grafana's build process is
# > build backend -> test backend -> build frontend -> test frontend
# while alpine doesn't allow easily interleaving those.
# Second problem is grafana doesn't support building on anything except linux/amd64,
# while alpine requires every build to be done natively.
# This is also a reason why we use prebuilt frontend archive.

pkgname=grafana-frontend
pkgver=9.0.3
pkgrel=0
pkgdesc="Open source, feature rich metrics dashboard and graph editor (frontend files)"
url="https://grafana.com"
arch="noarch"
license="AGPL-3.0-only"
options="!check" # We don't build frontend from sources.
source="$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz"
builddir="$srcdir/grafana-$pkgver"

package() {
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/plugins-bundled" "$builddir/public" "$pkgdir/usr/share/grafana/"
}

sha512sums="
1704dbddc22c9f6f07388df8ccaf4162c5cb93980aa3ca4bf8b2f0f8d1fe57385232da08bb621b64ab9fb249b37e806c1a8cc214f09ae657ede7945d5d1caf86  grafana-frontend-9.0.3-bin.tar.gz
"
